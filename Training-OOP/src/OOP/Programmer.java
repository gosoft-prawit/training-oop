package OOP;

public class Programmer extends Employee implements IWebsiteCreator, IWindowsInstaller {
	public Programmer(String firstnameInput, String lastnameInput, int salaryInput) {
		super(firstnameInput, lastnameInput, salaryInput);
	}

	public void FixPC(String serialNumber) {
		System.out.println("I'm trying to fix PC serialNumber:" + serialNumber);
	}

	public void createWebsite(String template, String titleName) {
		System.out.println("Setup template: " + template);
		System.out.println("Set Title name: " + titleName);
	}

	// เพิ่ม Method formatWindows จากไฟล์ IWindowsInstaller.java
	// เพิ่ม Method installWindows จากไฟล์ IWindowsInstaller.java
	// เพิ่ม Method getLastInstalledWindowsVersion จากไฟล์ IWindowsInstaller.java
	// โดย return เป็นตัวเลขค่าคงทีไปเลยว่าเป็น 10
	@Override
	public void formatWindows(String drive) {
		// TODO Auto-generated method stub

	}

	@Override
	public void installWindows(String version, String productKey) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getLastInstalledWindowsVersion() {
		// TODO Auto-generated method stub
		return 10;
	}
}
