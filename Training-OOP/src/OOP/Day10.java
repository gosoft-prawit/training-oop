package OOP;

import java.util.ArrayList;

public class Day10 {
	public static void main(String[] args) {
//	       Employee2 employee1 = new Employee2();
//	       System.out.println(employee1.firstname); // Anonymous
//	       System.out.println(employee1.lastname); // Anonymous
//	       System.out.println(employee1.getSalary());  // 9000
//
//	       Employee2 employee2 = new Employee2("Dang", "Red");
//	       System.out.println(employee2.firstname); // Dang
//	       System.out.println(employee2.lastname); // Red
//	       System.out.println(employee2.getSalary()); // 9000
//
//	       Employee2 employee3 = new Employee2("Dang", "Red", 20000);
//	       System.out.println(employee3.firstname); // Dang
//	       System.out.println(employee3.lastname); // Red
//	       System.out.println(employee3.getSalary()); // 20000

//	       CEO ceo = new CEO("Captain", "Marvel", 30000);
//	       Programmer dang = new Programmer("Dang", "Red", 10000);
//	       AI alphaGo = new AI("alphaGo", "Java");
//
//	       ceo.orderWebsite(dang);
//	       // Setup template: some template
//	       // Set Title name: Codecamp3       
//	       ceo.orderWebsite(alphaGo);
//	       // Java automated Setup template: some template
//	       // Java automated Set Title name: Codecamp3

		Employee dang = new Employee("Dang", "Red", 10000);
		Employee ceo = new CEO("Captain", "Marvel", 30000);
		Programmer prog = new Programmer("Captain", "Marvel", 20000);
		CEO ceo2 = (CEO) ceo;
		// CEO ceo3 = new Employee("Captain", "Marvel", 30000); // Error
		// CEO ceo4 = (CEO) dang; // Error
		ArrayList<Employee> myList = new ArrayList<>();
		myList.add(ceo2);
		myList.add(dang);
		myList.add(prog);
		CEO ceo3 = (CEO) myList.get(0);
		Employee dang2 = myList.get(1);
		Programmer prog2 = (Programmer) myList.get(2);
		ceo3.hello(); // Hi, nice to meet you. Captain!
		prog2.hello(); // Hello Captain

	}
}
