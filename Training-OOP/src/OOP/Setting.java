package OOP;

public class Setting {
	   public static float volumePercent = 50f;
	   public static String backgroundColor = "Black";
	   public static float brightnessPercent = 30f;
	   public static boolean isGpsOn = false;
	 
	   public static void TurnOnGPS() {
	       isGpsOn = true;
	   }
	   public static void TurnOffGPS() {
	       isGpsOn = false;
	   }
	   public static void addVolumePercent(float percent) {
	       volumePercent = volumePercent + percent;
	   }
	}
